const api_key = "47c5006c636083e12a50ff10893ecab7";
let strSearch="";

function loading(){
    $("ul").empty();
    $("#main").empty();
    $("#main").append(`
    <div class="text-center">
        <div class="spinner-grow text-primary" role="status">
            <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-secondary" role="status">
            <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-success" role="status">
            <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-danger" role="status">
            <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-warning" role="status">
            <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-info" role="status">
            <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-light" role="status">
            <span class="sr-only">Loading...</span>
        </div>
        <div class="spinner-grow text-dark" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>`
    );
}

async function loadTopRated(){
    let reqStr = `https://api.themoviedb.org/3/movie/top_rated?api_key=${api_key}`;

    loading();

    let response = await fetch(reqStr);
    let rs = await response.json(); 
    
    fillMovies(rs,1);
}

async function evtSubmit(e){
    e.preventDefault();
    loading();

    strSearch = $("form input").val();

    if (strSearch!=""){
        let reqStr = `https://api.themoviedb.org/3/search/multi?api_key=${api_key}&query=${strSearch}`;
        let response = await fetch(reqStr);
        let result = await response.json();

        fillMovies(result,2);
    }else{
        loadTopRated();
    }
}

async function fillMovies(result,type){
    loading();
    let rs = [];
    if (type == 1){
        rs = result.results;
    }else{
        for (let r of result.results){
            if (r.media_type=="movie"){
                rs.push(r);
            }else if(r.media_type=="person"){
                for (let m of r.known_for){
                    rs.push(m);
                }
            }
        }
    }
    

    $("#main").empty();

    if (rs.length==0){
        $("#main").append(`<div class="center">Không có dữ liệu</div>`);
        return;
    }
    $("#main").append(`<div class="row" id="card_film"><div>`);
    for (const m of rs){
       $('#card_film').append(`
            <div class="card" style="width: 18rem; margin: 2rem;">
                <img src="https://image.tmdb.org/t/p/w500${m.poster_path}"
                    class="img-thumbnail" class="card-img-top" alt="${m.title}">
                <div class="card-body">
                    <h5>${m.title}</h5>
                    
                    <h6>Vote average: ${m.vote_average} (${m.vote_count} vote)</h6>
                    <p class="card-text">${m.overview}</p>
                </div>
                <div class="card-foot">
                    <button class="btn btn-primary" style="width:18rem"
                        onclick="loadMovie(${m.id},event);">Detail</button>
                </div>
            </div>
        `);
    }

    let page = result.page;
    let start = page - 2;
    let end = result.total_pages;
    if (start > end - 4){start = end - 4;}
    if (start < 1){
        start = 1;
    }
    for (let i = start;i<=start+4 && (i<=end);i++){
        if (i == page){
            $('ul').append(`
            <li class="page-item active">
                <a class="page-link" href="#" onclick="getSearchPage(${i},event);">${i}</a>
            </li>
            `)
        }else{
            $('ul').append(`
            <li class="page-item">
                <a class="page-link" href="#" onclick="getSearchPage(${i},event);">${i}</a>
            </li>
            `)
        }
    }
}

async function getSearchPage(page,e){
    e.preventDefault();
    loading();
    
    if (strSearch==""){
        let reqStr = `https://api.themoviedb.org/3/movie/top_rated?api_key=${api_key}&page=${page}`;
        let response = await fetch(reqStr);
        let rs = await response.json();

        fillMovies(rs,1);
    }else{
        let reqStr = `https://api.themoviedb.org/3/search/multi?api_key=${api_key}&query=${strSearch}&page=${page}`;
        let response = await fetch(reqStr);
        let rs = await response.json();

        fillMovies(rs,2);
    }
}

async function loadMovie(id,e){
    e.preventDefault();
    loading();
    let reqStr = `https://api.themoviedb.org/3/movie/${id}?api_key=${api_key}`;
    let response = await fetch(reqStr);
    let rs = await response.json();

    let reqStrCasts = `https://api.themoviedb.org/3/movie/${id}/credits?api_key=${api_key}`;
    let responeCasts = await fetch(reqStrCasts);
    let rsCasts = await responeCasts.json();

    $("#main").empty();
    $("#main").append(`
    <div class="container-fluid">
        <img src="https://image.tmdb.org/t/p/w500${rs.poster_path}"
            style="width: 22rem; height: 30rem; 
            margin:3rem" class="float-right"/>
        <br/><br/><br/>
        <h5 class="d-inline">Title: </h5>${rs.title} (${rs.original_title})<br/><br/>
    </div>
    `);
    for (let i of rsCasts.crew){
        if (i.job == "Director"){
            $(".container-fluid").append(`
            <h5 class="d-inline">Director: </h5>${i.name}<br/><br/>
            `)
        }
    }

    $(".container-fluid").append(`
        <h5 class="d-inline">Status: </h5>${rs.status}<br/><br/>
        <h5 class="d-inline">Release date: </h5>${rs.release_date}<br/><br/>
        <h5 class="d-inline">Runtime: </h5>${rs.runtime} min<br/><br/>
        <h5 class="d-inline">vote average: </h5>${rs.vote_average} (${rs.vote_count} vote)<br/><br/>
        <h5 class="d-inline">Genres: </h5><p class="d-inline" id="genres"></p><br/><br/>
        <h5 class="d-inline">Overview: </h5>${rs.overview}<br/><br/>
        <h5 class="d-inline">Cast: </h5><p id="cast"><p><br/><br/>
        <h5 class="d-inline">Reviews: </h5><p id="reviews"><p><br/><br/>
    `)
    for (let i of rsCasts.cast) {
        $("#cast").append(`
            <a style="font-weight:bold;color:black;" href="#" 
                onclick="loadCaster(${i.id},event);">
                ${i.name}
            </a>, 
        `)
    }
    for (let i of rs.genres){
        $("#genres").append(`${i.name}, `);
    }
    getReviewsPage(id,1);
}

async function getReviewsPage(id,page,e) {
    e = e || 0;
    if (e!=0){
        e.preventDefault();
    }
   
    let reqReviews = `https://api.themoviedb.org/3/movie/${id}/reviews?api_key=${api_key}&page=${page}`;
    let responseReviews = await fetch(reqReviews);
    let rsReviews = await responseReviews.json();

    for (let i of rsReviews.results){
        $("#reviews").append(`
            <h5 class="d-inline">${i.author}: </h5>${i.content}<br><br>
        `);
    }

    let start = page - 2;
    if (start < 1){
        start = 1;
    }
    for (let i = start;i<=start+5 && i<=rsReviews.total_pages;i++){
        if (i == page){
            $('ul').append(`
            <li class="page-item active">
                <a class="page-link" href="#" onclick="getReviewsPage(${id},${i},event);">${i}</a>
            </li>
            `);
        }else{
            $('ul').append(`
            <li class="page-item">
                <a class="page-link" href="#" onclick="getReviewsPage(${id},${i},event);">${i}</a>
            </li>
            `);
        }
    };
}

async function loadCaster(cast_id,e) {
    e.preventDefault();
    loading();
    let reqStr = `https://api.themoviedb.org/3/person/${cast_id}?api_key=${api_key}`;
    let response = await fetch(reqStr);
    let rs = await response.json();

    $("#main").empty();
    $("#main").append(`
    <div class="container-fluid">
        <img src="https://image.tmdb.org/t/p/w500${rs.profile_path}"
            style="width: 22rem; height: 30rem; 
            margin:3rem" class="float-right"/>
        <br/><br/><br/>
        <h1 class="d-inline">${rs.name}</h1><br/><br/>
        <h5 class="d-inline">Birthday: </h5>${rs.birthday}<br><br>
        <h5 class="d-inline">Place of birth: </h5>${rs.place_of_birth}<br/><br/>
        ${rs.biography}<br><br>
        <button class="btn btn-outline-success my-2 my-sm-0 d-inline" onclick="fillMoviesCast(${cast_id},1);">${rs.name}'s Movies</button><br/><br/>
    </div>
    `);
}

async function fillMoviesCast(cast_id,page,e){
    loading();
    e = e || 0;
    if (e!=0){e.preventDefault();}
    let reqMovies = `https://api.themoviedb.org/3/person/${cast_id}/movie_credits?api_key=${api_key}`;
    let responseMovies = await fetch(reqMovies);
    let movies = await responseMovies.json();

    $("#main").empty();
    $("#main").append(`<div class="row" id="card_film"><div>`);
    let index = (page-1)*20;
    while ((index < page*20) && (index < movies.cast.length)){
        let m = movies.cast[index];
        $('#card_film').append(`
            <div class="card" style="width: 18rem; margin: 2rem;">
                <img src="https://image.tmdb.org/t/p/w500${m.poster_path}"
                    class="img-thumbnail" class="card-img-top" alt="${m.title}">
                <div class="card-body">
                    <h5>${m.title}</h5>
                    
                    <h6>Vote average: ${m.vote_average} (${m.vote_count} vote)</h6>
                    <p class="card-text">${m.overview}</p>
                </div>
                <div class="card-foot">
                    <button class="btn btn-primary" style="width:18rem"
                        onclick="loadMovie(${m.id},event);">Detail</button>
                </div>
            </div>
        `);
        index++;
    }

    let start = page - 2;
    let end = parseInt((movies.cast.length-1)/20+1);
    if (start > end - 4){start = end - 4;}
    if (start < 1){start = 1;}
    for (let i = start;(i<=start + 4) && (i<= end);i++){
        if (i == page){
            $('ul').append(`
            <li class="page-item active">
                <a class="page-link" href="#" onclick="fillMoviesCast(${cast_id},${i},event);">${i}</a>
            </li>
            `);
        }else{
            $('ul').append(`
            <li class="page-item">
                <a class="page-link" href="#" onclick="fillMoviesCast(${cast_id},${i},event);">${i}</a>
            </li>
            `);
        }
    };
}